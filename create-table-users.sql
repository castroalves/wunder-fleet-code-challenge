CREATE TABLE IF NOT EXISTS users (
  user_id INTEGER PRIMARY KEY,
  first_name TEXT NOT NULL,
  last_name TEXT NOT NULL,
  phone TEXT NOT NULL,
  street TEXT NOT NULL,
  house_number TEXT,
  zip_code TEXT NOT NULL,
  city TEXT NOT NULL,
  account_owner TEXT NOT NULL,
  iban TEXT NOT NULL
);
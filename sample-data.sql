INSERT INTO users (
    first_name,
    last_name,
    phone,
    street,
    house_number,
    zip_code,
    city,
    account_owner,
    iban
  )
VALUES (
    'Cadu',
    'de Castro Alves',
    '4915259031890',
    'Schenkendorfst',
    '39',
    '04275',
    'Leipzig',
    'Cadu de Castro Alves',
    'DE1234567890'
);
# Tasks - Wunder Fleet Coding Challenge

-   [x] Create MVP
-   [x] Change to MVC architecture
    -   [x] Models/Database.php
    -   [x] Models/User.php
    -   [x] Views/PaymentView.php
    -   [x] Controllers/PaymentController.php
    -   [x] Controllers/UserController.php
-   [x] If user leave at any step, save currentStep and userData (localStorage, cookies, cache, etc)
-   [x] If user come back, open form on saved currentStep and with userData
-   [x] Add error message in case of failed payment

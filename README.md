# README

## Description

This repository was created by Carlos Eduardo Mendes de Castro Alves for Wunder Fleet Code Challenge.

## Setup Instructions

### Database

This app uses SQLite3. The DB - `data.sqlite` - is included in the repo with dummy data.

To setup a clear DB:

1. Recreate `data.sqlite` in the app root.
2. Run `create-table-users.sql` to create users table.

### App

1. `git clone repo`
2. `composer install`

## Usage

Run `composer run` (or `php -S localhost:8080`) on Terminal.

Open http://localhost:8080 and you should be good to go.

### Testing "cart abandonment"

The app uses `LocalStorage API` to store user input data from step 1 and 2 (Personal and Address Information). I've decided to not store payment data for later usage because it's a very sensitive information.

For privacy purposes, LocalStorage data are cleared after payment is submitted. Step cookie is also reset at the end.

Whenever you want to restart this feature, just click on [Reset](http://localhost:8080/?reset=1) link (above Wunder Fleet "logo"). Just remember to reload the page without this querystring, otherwise the app will never work.

## Challenge Questions

### Performance optimizations

    -   Queueus to process payment registrations
    -   Cache currentStep and userData server side (APCu, Memcached, Redis etc), alongside user IP
    -   MySQL/MariaDB instead of SQLite

### Which things could be done better?

    -   Store client data using Cookies, so that we could control data expiration (LocalStorage doesn't support that)
    -   Server side validation
    -   Implementation of routing system
    -   MySQL and a DB Library (PDO/Doctrine)
    -   Write JS script in TypeScript for better compatibility (especially old browsers)
    -   Axios for API calls (compatibility with old browser)
    -   DocBlock documentation and PHP 7.4+ features
    -   UX
        -   On Step 3, if Account Owner is same as First and Last Names, add a button to copy this information
        -   Display the current step on the page
        -   Allow user to go to any step and change information
        -   App Confirmation step to display all provided information before hitting "Pay"
        -   Display loading indicator

<?php

require_once('vendor/autoload.php');

use WunderFleet\Controllers\PaymentController;
use WunderFleet\Controllers\UserController;

$allowedActions = ['pay', 'save'];

$action = isset($_GET['action']) ? $_GET['action'] : null;
$postData = json_decode(file_get_contents('php://input'), true);

if (in_array($action, $allowedActions) && is_array($postData)) {

    switch ($action) {
        case 'pay':
            $payment = new PaymentController;
            echo $payment->pay($postData);

            break;
        case 'save':

            $user = new UserController;
            echo $user->create($postData);

            break;
        default:
            throw new Exception("Unhandled action");
            break;
    }
} else {
    http_response_code(405);
    echo '🚴‍♂️ Bike is freedom...';
}

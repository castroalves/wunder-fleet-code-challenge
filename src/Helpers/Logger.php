<?php

namespace WunderFleet\Helpers;

class Logger
{
    public static function output($value)
    {
        error_log(
            print_r($value, true)
        );
    }
}
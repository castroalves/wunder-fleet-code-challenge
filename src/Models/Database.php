<?php

namespace WunderFleet\Models;

use Exception;
use \SQLite3;
use SQLite3Result;
use WunderFleet\Helpers\Logger;

/**
 * @author Cadu de Castro Alves <cadudecastroalves@gmail.com>
 */
class Database
{
    private $database = '/../data.sqlite';

    private $db;

    private $stmt;

    /**
     * Constructor
     */
    protected function __construct()
    {
        try {
            $this->db = new SQLite3(dirname(__DIR__) . $this->database, SQLITE3_OPEN_READWRITE);
        } catch(Exception $e) {
            Logger::output([
                'message' => $e->getMessage(),
                'database' => $this->database,
            ]);
            throw new Exception('Database not found!');
        }
        
    }

    /**
     * Prepare DB query
     *
     * @param string $sql
     * @return void
     */
    public function query(string $sql): void
    {
        $this->stmt = $this->db->prepare($sql);
    }

    /**
     * Bind SQL query parameters
     *
     * @param string $param
     * @param mixed $value
     * @return void
     */
    public function bind(string $param, $value): void
    {
        $this->stmt->bindValue($param, $value);
    }

    /**
     * Execute DB query
     *
     * @return SQLite3Result
     */
    public function execute(): SQLite3Result
    {
        return $this->stmt->execute();
    }

    /**
     * Fetch all data from database
     *
     * @return array
     */
    public function fetchAll(): array
    {
        $result = $this->execute();
        return $result->fetchArray(SQLITE3_ASSOC);
    }

    /**
     * Returns last inserted row ID
     *
     * @return integer
     */
    public function lastInsertRowID(): int
    {
        return $this->db->lastInsertRowID();
    }
}
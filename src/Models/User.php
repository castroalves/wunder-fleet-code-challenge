<?php

namespace WunderFleet\Models;

use Exception;
use WunderFleet\Helpers\Logger;
use WunderFleet\Models\Database;

class User extends Database
{
    public $firstName;
    public $lastName;
    public $phone;
    public $street;
    public $houseNumber;
    public $zipCode;
    public $city;
    public $accountOwner;
    public $iban;

    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function create()
    {
        $this->db->query('
            INSERT INTO 
            users (first_name, last_name, phone, street, house_number, zip_code, city, account_owner, iban)
            values (:first_name, :last_name, :phone, :street, :house_number, :zip_code, :city, :account_owner, :iban)
        ');

        $this->db->bind(':first_name', $this->firstName);
        $this->db->bind(':last_name', $this->lastName);
        $this->db->bind(':phone', $this->phone);
        $this->db->bind(':street', $this->street);
        $this->db->bind(':house_number', $this->houseNumber);
        $this->db->bind(':zip_code', $this->zipCode);
        $this->db->bind(':city', $this->city);
        $this->db->bind(':account_owner', $this->accountOwner);
        $this->db->bind(':iban', $this->iban);

        $result = $this->db->execute();

        if ($result) {
            return $this->db->lastInsertRowID();
        } else {
            return false;
        }
    }

    public function getAll()
    {
        $this->db->query('SELECT * FROM users');
        return $this->db->fetchAll();
    }

    public function get(int $userId)
    {
        $this->db->query('SELECT * FROM users WHERE user_id = :user_id');
        $this->db->bind(':user_id', $userId);

        $result = $this->db->execute();
        if ($result) {
            return $result->fetchAll();
        } else {
            return [];
        }
    }

    public function toArray()
    {
        return (array) $this;
    }
}
<?php

namespace WunderFleet\Controllers;

use WunderFleet\Models\User;
use WunderFleet\Helpers\Logger;

class UserController
{
    private $user;

    public function __construct()
    {
        $this->user = new User;
    }

    public function create(array $data)
    {
        $this->user->firstName = trim($data['firstName']);
        $this->user->lastName = trim($data['lastName']);
        $this->user->phone = trim($data['phone']);
        $this->user->street = trim($data['street']);
        $this->user->houseNumber = trim($data['houseNumber']);
        $this->user->zipCode = trim($data['zipCode']);
        $this->user->city = trim($data['city']);
        $this->user->accountOwner = trim($data['accountOwner']);
        $this->user->iban = trim($data['iban']);

        $userId = $this->user->create();

        if ($userId) {
            Logger::output([
                'message' => 'User has been saved!',
                'user' => $this->user->toArray(),
            ]);

            return json_encode([
                'customerId' => $userId,
                'owner' => $this->user->accountOwner,
                'iban' => $this->user->iban,
            ]);
        } else {
            Logger::output([
                'error' => 'User data could not be saved!',
                'user' => $this->user->toArray(),
            ]);

            return json_encode([
                'error' => 'User data could not be saved!',
            ]);
        }
    }
}

<?php

namespace WunderFleet\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use WunderFleet\Helpers\Logger;
use GuzzleHttp\Psr7\Message;

class PaymentController
{
    private $url = "https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data";

    public function __construct()
    {
    }

    public function pay(array $data)
    {
        $client = new Client();
        
        try {
            $response = $client->post($this->url, [
                'json' => $data
            ]);
            return $response->getBody();
        } catch(ClientException $e) {
            Logger::output([
                'message' => $e->getMessage(),
            ]);
            return json_encode([
                'error' => 'POST expects the following structure: {"customerId": 1,"iban":"DE1234","owner":"Max Musterman"}',
                'submitted_data' => $data,
            ]);
        }
        
    }
}
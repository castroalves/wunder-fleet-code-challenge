<?php

namespace WunderFleet\Views;

use WunderFleet\Helpers\Logger;

/**
 * @author Cadu de Castro Alves <cadudecastroalves@gmail.com>
 */
class PaymentView
{
    public function __construct()
    {
        // 
    }

    /**
     * Render view
     *
     * @return void
     */
    public static function render(): void
    {
        $step = $_REQUEST['step'] ?? 1;
        
        include_once(__DIR__ . '/../../public/header.php');
        include_once(__DIR__ . '/../../public/payment-form.php');
        include_once(__DIR__ . '/../../public/footer.php');
    }
}
<div id="wrapper" class="container mx-auto w-1/2 mt-10">
  <a class="btn-reset" href="?reset=1">Restart</a>
  <h1 class="font-bold text-5xl mb-4">Wunder Fleet</h1>
  <h2 class="font-bold text-3xl mb-4">Signup</h2>
  <div id="step-1" class="step hidden">
    <h3 class="font-bold text-2xl mb-4">Personal Information</h3>
    <form method="POST" name="user-data">
      <div class="form-row">
        <label for="first-name">First Name</label>
        <input type="text" id="first-name" name="first_name" placeholder="First Name" value="<?php echo $_REQUEST['first_name'] ?? null; ?>" autocomplete="off" required>
      </div>
      <div class="form-row">
        <label for="last-name">Last Name</label>
        <input type="text" id="last-name" name="last_name" placeholder="Last Name" value="<?php echo $_REQUEST['last_name'] ?? null; ?>" autocomplete="off" required>
      </div>
      <div class="form-row">
        <label for="phone">Phone</label>
        <input type="text" id="phone" name="phone" placeholder="Phone Number" value="<?php echo $_REQUEST['phone'] ?? null; ?>" autocomplete="off" required>
      </div>
      <div class="form-row">
        <button type="submit" class="btn-next" data-step="2">Next</button>
      </div>
    </form>
  </div>
  <div id="step-2" class="step hidden">
    <h3 class="font-bold text-2xl mb-4">Address Information</h3>
    <form method="POST" name="address-data">
      <div class="form-row">
        <label for="street">Street</label>
        <input type="text" id="street" name="street" value="<?php echo $_REQUEST['street'] ?? null; ?>" autocomplete="off" required>
      </div>
      <div class="form-row">
        <label for="house-number">House Number</label>
        <input type="text" id="house-number" name="house_number" value="<?php echo $_REQUEST['house_number'] ?? null; ?>" autocomplete="off" required>
      </div>
      <div class="form-row">
        <label for="zip-code">Zipcode</label>
        <input type="text" id="zip-code" name="zip_code" pattern="[0-9]{5}" value="<?php echo $_REQUEST['zipcode'] ?? null; ?>" title="Five digit zip code" autocomplete="off" required>
      </div>
      <div class="form-row">
        <label for="city">City</label>
        <input type="text" id="city" name="city" value="<?php echo $_REQUEST['city'] ?? null; ?>" autocomplete="off" required>
      </div>
      <div class="form-row">
        <button type="submit" class="btn-next" data-step="3">Next</button>
      </div>
    </form>
  </div>
  <div id="step-3" class="step hidden">
    <h3 class="font-bold text-2xl mb-4">Payment Information</h3>
    <form method="POST" name="payment-data">
      <div class="form-row">
        <label for="account-owner">Account Owner</label>
        <input type="text" id="account-owner" name="account_owner" value="<?php echo $_REQUEST['account_owner'] ?? null; ?>" autocomplete="off" required>
      </div>
      <div class="form-row">
        <label for="iban">IBAN</label>
        <input type="text" id="iban" name="iban" value="<?php echo $_REQUEST['iban'] ?? null; ?>" autocomplete="off" required>
      </div>
      <div class="form-row">
        <button type="submit" class="btn-next" data-step="4">Pay</button>
      </div>
    </form>
  </div>
  <div id="step-4" class="step hidden">
    <div id="success" class="hidden">
        <h3 class="font-bold text-2xl mb-4">Confirmation</h3>
        <div class="success bg-green-50 border-green-400 border-2 mb-4 p-4 rounded">
        <p class="font-bold text-xl">Thanks! Your data has been saved!</p>
        <p><strong>Payment ID:</strong> <span class="payment-data-id">007f3eb67b50475f9de628bfdfd5244d3096959800a70b9f250ecd4220e9e4e2f215faeab9715168f1c31737904cb5c3</span> </p>
        </div>
    </div>
    <div id="error" class="hidden">
        <h3 class="font-bold text-2xl mb-4">Uh oh!</h3>
        <div class="success bg-red-50 border-red-400 border-2 mb-4 p-4 rounded">
            <p class="font-bold text-xl">Your payment could not be processed!</p>
        </div>
    </div>
      <div class="form-row">
        <button class="btn-reset">Try Again</button>
      </div>
  </div>
</div>
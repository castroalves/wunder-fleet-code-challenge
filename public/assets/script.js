const wfSignup = (() => {
    console.log("Wunder Fleet Signup Form has been loaded!");

    // Initial step
    let step = "1";

    const setCookie = (cname, cvalue, exdays) => {
        let expires = "";
        if (exdays) {
            const d = new Date();
            d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
            expires = "expires=" + d.toUTCString();
        }
        const path = document.location.search !== "" ? document.location.search : "/";
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=" + path;
    };

    const getCookie = (cname) => {
        const name = cname + "=";
        const ca = document.cookie.split(";");
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == " ") {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return null;
    };

    const setStepState = (value) => {
        step = value;
    };

    const getCurrentStep = () => {
        return step;
    };

    const displayStep = () => {
        const currentStep = getParam("step") || getCurrentStep();
        console.log("displayStep currentStep => " + currentStep);
        const steps = document.querySelectorAll(".step");
        steps.forEach((el) => {
            const nextStepId = "step-" + currentStep;
            if (el.id === nextStepId) {
                el.classList.remove("hidden");
                el.classList.add("active");
            } else {
                el.classList.remove("active");
                el.classList.add("hidden");
            }
        });
    };

    const saveToDatabase = async (userData) => {
        console.log("Saving to Database...");
        fetch("/api.php?action=save", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(userData),
        })
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                console.log("User data has been saved!", data);
                window.localStorage.clear();
                console.log("LocalStorage has been cleared!");
                submitPayment(data);
            })
            .catch((error) => {
                console.log("User data could not be saved!", error);
            });
    };

    const submitPayment = async (paymentData) => {
        console.log("Submiting payment...");
        fetch("/api.php?action=pay", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(paymentData),
        })
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                showLastStep(data);
            })
            .catch((error) => {
                console.log("Payment failed!", error);
            });
    };

    const showLastStep = (data) => {
        console.log("showLastStep", data);
        if (data.error) {
            console.log("Payment failed!", data.error);
            const error = document.getElementById("error");
            error.classList.toggle("hidden");
        } else {
            if (data.paymentDataId !== "") {
                const success = document.getElementById("success");
                success.classList.toggle("hidden");

                const paymentDataIdElement = document.getElementsByClassName("payment-data-id")[0];
                paymentDataIdElement.innerHTML = data.paymentDataId;
            }
        }
    };

    const updateFormData = (isSave) => {
        const currentStep = getCurrentStep();
        console.log("Current Step => " + currentStep);

        const localStorage = window.localStorage;

        const firstName = document.getElementById("first-name");
        const lastName = document.getElementById("last-name");
        const phone = document.getElementById("phone");
        const street = document.getElementById("street");
        const houseNumber = document.getElementById("house-number");
        const zipCode = document.getElementById("zip-code");
        const city = document.getElementById("city");
        const accountOwner = document.getElementById("account-owner");
        const iban = document.getElementById("iban");

        console.log("Loaded step " + currentStep);

        console.log("Local Storage", localStorage);

        switch (currentStep) {
            // Personal Information
            case "1":
                // Do nothing
                break;
            // Address Information
            case "2":
                // Save user data in localStorage
                if (isSave) {
                    localStorage.setItem("firstName", firstName.value);
                    localStorage.setItem("lastName", lastName.value);
                    localStorage.setItem("phone", phone.value);

                    console.log("User data saved in localStorage", localStorage);
                }

                // Focus on first field
                street.focus();

                // Load data from localStorage

                break;
            // Payment Information
            case "3":
                // Save address in localStorage
                if (isSave) {
                    localStorage.setItem("street", street.value);
                    localStorage.setItem("houseNumber", houseNumber.value);
                    localStorage.setItem("zipCode", zipCode.value);
                    localStorage.setItem("city", city.value);

                    console.log("Address saved in localStorage", localStorage);
                }

                // Focus on first field
                accountOwner.focus();

                break;
            case "4":
                // Save payment in localStorage
                localStorage.setItem("accountOwner", accountOwner.value);
                localStorage.setItem("iban", iban.value);

                console.log("Address saved in localStorage", localStorage);
                saveToDatabase(localStorage);
                break;
            default:
                console.log("Unhandled step");
                break;
        }

        // Load data saved in Local Storages
        firstName.value = localStorage.getItem("firstName");
        lastName.value = localStorage.getItem("lastName");
        phone.value = localStorage.getItem("phone");
        street.value = localStorage.getItem("street");
        houseNumber.value = localStorage.getItem("houseNumber");
        zipCode.value = localStorage.getItem("zipCode");
        city.value = localStorage.getItem("city");
    };

    const getParam = (param) => {
        const urlParams = new URLSearchParams(window.location.search);
        return urlParams.get(param);
    };

    const resetData = () => {
        setCookie("step", "", -1);
        window.localStorage.clear();
        console.log("cookies and localStorage have been cleared");
    };

    const init = () => {
        // Reset cookie (for testing purposes)
        const reset = getParam("reset");
        if (reset) {
            // Reset cookies and LocalStorage
            resetData();
        }

        console.log(document.cookie);

        // Get stepCookie
        let stepCookie = getCookie("step");
        const firstStep = 1;
        const daysToExpire = 30;

        // Check stepCookie >= 1
        // If so, load stored step
        // Otherwise, initialize stepCookie
        if (stepCookie !== null && stepCookie >= firstStep) {
            setStepState(stepCookie);
        } else {
            setCookie("step", firstStep, daysToExpire);
        }

        // Load form data
        updateFormData(false);

        // Load
        displayStep();

        // Focus on first name
        document.getElementById("first-name").focus();

        const btnNext = document.querySelectorAll(".btn-next");
        btnNext.forEach((el) => {
            el.addEventListener("click", (e) => {
                e.preventDefault();

                let form = e.currentTarget.form;
                let formIsValid = form.reportValidity();

                if (formIsValid) {
                    const nextStep = e.currentTarget.dataset.step;

                    // Set to next step
                    setStepState(nextStep);

                    // Display current step
                    displayStep();

                    // Update data
                    updateFormData(true);

                    // Update step cookie
                    setCookie("step", nextStep, 30);
                }
            });
        });

        const btnReset = document.querySelectorAll(".btn-reset");
        btnReset.forEach((el) => {
            el.addEventListener("click", (e) => {
                e.preventDefault();

                // Clear cookies and LocalStorage
                resetData();

                // Reload first step
                window.location.replace(window.location.href);
            });
        });
    };

    return {
        init,
    };
})();

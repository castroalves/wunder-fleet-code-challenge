<?php

include_once('vendor/autoload.php');

use WunderFleet\Views\PaymentView;

$paymentForm = new PaymentView;
$paymentForm::render();
